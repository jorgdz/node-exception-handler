'use strict'

const { NotFoundException } = require('../src/exceptions/exceptions')

var products = [
  {
    id: 1,
    name: 'Teclado',
    price: 12.50
  },
  {
    id: 2,
    name: 'Mouse',
    price: 6.50
  },
  {
    id: 3,
    name: 'Monitor',
    price: 30
  },
  {
    id: 4,
    name: 'Equipo de cómputo (Escritorio)',
    price: 600
  }
]

module.exports = {
  getProducts () {
    return Promise.resolve(products)
  },

  async getProduct (id) {
    const product = await products.find(p => p.id === parseInt(id))

    if (product === undefined) {
      throw new NotFoundException('Product not found.')
    }

    return Promise.resolve(product)
  }
}

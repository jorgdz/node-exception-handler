'use strict'

const test = require('ava')
const fixtures = require('../fixtures')

test('get all products', async t => {
  var products = await fixtures.getProducts()

  t.deepEqual(products, [{
    id: 1,
    name: 'Teclado',
    price: 12.50
  },
  {
    id: 2,
    name: 'Mouse',
    price: 6.50
  },
  {
    id: 3,
    name: 'Monitor',
    price: 30
  },
  {
    id: 4,
    name: 'Equipo de cómputo (Escritorio)',
    price: 600
  }])
})

test('get product by id', async t => {
  var product = await fixtures.getProduct(2)

  t.deepEqual(product, {
    id: 2,
    name: 'Mouse',
    price: 6.50
  })
})

test('not found product by id', async t => {
  await t.throwsAsync(async () => {
    await fixtures.getProduct(32)
  })
})

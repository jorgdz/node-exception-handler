'use strict'

const test = require('ava')
const { validateDecimalPrecition, checkNumber } = require('../src/exceptions/lib')

test('validate 2 precition decimal', t => {
  t.truthy(validateDecimalPrecition(10.70))
})

test('fail validate 2 precition decimal', t => {
  t.throws(() => {
    validateDecimalPrecition(23.9001)
  })
})

test('validate integer', t => {
  t.truthy(checkNumber(12))
})

test('fail validate integer', t => {
  t.throws(() => {
    checkNumber('adasa')
  })
})

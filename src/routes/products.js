const express = require('express')
const router = express.Router()
const fixtures = require('../../fixtures')
const { checkNumber, validateFields } = require('../exceptions/lib')

// GET product /:id
router.get('/products/:id', async function (req, res, next) {
  try {
    checkNumber(req.params.id)
    const product = await fixtures.getProduct(req.params.id)

    res.status(200).send(product)
  } catch (err) {
    next(err)
  }
})

// GET products /
router.get('/products', async function (req, res, next) {
  const products = await fixtures.getProducts()
  res.status(200).send(products)
})

// ADD product /
router.post('/products', async function (req, res, next) {
  try {
    const body = req.body

    // Validate fields
    await validateFields(body, {
      name: 'required|maxLength:25',
      price: 'required|decimal'
    })

    // Go code
    console.log('OK')
    res.status(201).send({ data: body })
  } catch (err) {
    next(err)
  }
})

module.exports = router

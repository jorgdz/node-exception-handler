'use strict'

const niv = require('node-input-validator')
const messages = require('../utils/messages')
const { BadRequestException, ConflictException, MethodArgumentNotValidException } = require('../exceptions')

exports.checkNumber = (value) => {
  var RE = /^([0-9])*$/
  if (!RE.test(value)) {
    throw new BadRequestException(value + ' is not a number.')
  }

  return this
}

exports.checkIdGreaterThan = (id) => {
  if (id < 3) {
    throw new BadRequestException('Fail greater number.')
  }

  return this
}

// Validar un número decimal
exports.validateDecimal = (value) => {
  var RE = /^\d*\.?\d*$/
  if (!RE.test(value)) {
    throw new ConflictException('Invalid decimal.')
  }

  return this
}

// Validar un número decimal con dos dígitos de precisión
exports.validateDecimalPrecition = (value) => {
  var RE = /^\d*(\.\d{1})?\d{0,1}$/
  if (!RE.test(value)) {
    throw new ConflictException('Invalid precition decimal.')
  }

  return this
}

exports.validateFieldsProducts = (id, name, price) => {
  var arrayErrors = []

  if (id === undefined) {
    arrayErrors.push('El id es obligatorio')
  } else {
    var RE_NUMBER = /^([0-9])*$/
    if (!RE_NUMBER.test(id)) {
      arrayErrors.push('El id debe ser numérico')
    }
  }

  if (name === undefined || name === null || name === '') {
    arrayErrors.push('El nombre del producto es obligatorio')
  }

  if (price === undefined) {
    arrayErrors.push('El precio es obligatorio')
  } else {
    var RE_DECIMAL = /^\d*(\.\d{1})?\d{0,1}$/
    if (!RE_DECIMAL.test(price)) {
      arrayErrors.push(`El precio ${price} no puede tener más de 2 decimales`)
    }
  }

  if (arrayErrors.length > 0) {
    throw new MethodArgumentNotValidException('Error in product fields', arrayErrors)
  }

  return this
}

exports.validateFields = async (body, rules) => {
  niv.extendMessages(messages)

  const validator = new niv.Validator(body, rules)
  const matched = await validator.check()

  if (!matched) {
    throw new MethodArgumentNotValidException('Fields errors.', validator.errors)
  }
}
